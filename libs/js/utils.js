module.exports = {
	// 查找父级组件
	queryParent(name = undefined) {
		let parent = this.$parent;
		while (parent) {
			if (parent.$options && parent.$options.name !== name) {
				parent = parent.$options.parent;
			} else {
				return parent;
			}
		}
		return false
	},

	// 查找子级组件
	queryChilds(name = undefined, callback, childrens = []) {
		childrens = !childrens.length ? this.$children : childrens
		if (childrens && childrens.length) {
			childrens.forEach(child => {
				let childName = child.$options.name;
				if (childName === name) {
					callback && callback(child)
				} else if (child.$children) {
					module.exports.queryChilds(name, callback, child.$children);
				}
			})
		}
	}
}
