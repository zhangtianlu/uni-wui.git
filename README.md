## 前言

uni-wui 是使用 uni-app 开发的微信小程序,H5 端的 UI 组件库,组件高可用,可扩展,可商用

## 特性

-   组件高可用,方便扩展,开箱即用
-   注重业务组件的开发,让你的工作如虎添翼
-   按需引入，减少打包体积

## 预览

您可以通过**微信**扫码，查看最佳的演示效果

![Image text](https://resources.xwaitl.cn/md/WechatAppletCode.jpg)

## 源码地址

[uni-wui 组件库地址](https://gitee.com/zhangtianlu/uni-wui.git)

[小程序演示地址](https://gitee.com/zhangtianlu/uni-wui-demo.git)

文档作者正在写...

如果喜欢 uni-wui 可给个 Star,感谢!

## 交流

如果有什么技术问题,可以加作者微信,一起交流学习!

![Image text](https://resources.xwaitl.cn/md/WeChat.jpg)

也可以加入 QQ 群进行技术交流（群号：480607638）！

![Image text](https://resources.xwaitl.cn/md/qqgroup1.png)

## 开始使用

`npm` 安装 uni-wui

```
// 如果您的项目是HX创建的，根目录又没有package.json文件的话，请先执行如下命令：
// npm init -y

// 安装
npm install uni-wui

// 更新
npm update uni-wui
```

`main.js` 引入 uni-wui

```
import uniWui from 'uni-wui'
Vue.use(uniWui)
```

`App.vue` 引入 uni-wui 的样式

```
@import "uni-wui/index.scss";
```

(注意:uni-app 使用 scss 样式,请先安装 sass)
<br/>

---

## 使用"easycom"模式引入组件

使用"easycom"模式更方便组件使用,使用简单,而且是 uni-app 官方推荐的用法
<br>
<br>
`pages.json` 使用"easycom"引入 uni-wui

```
"easycom": {
	"autoscan": true,
	"custom": {
		"^w-(.*)": "uni-wui/components/w-$1/w-$1.vue"
	}
},
```

## 使用组件

`index.vue` 使用"w-button"组件测试 uni-wui 是否成功引入

```
<w-button type="primary">默认按钮</w-button>
```
