import config from './config'
import utils from './libs/js/utils.js'

const install = (Vue) => {
	Vue.prototype.systemInfo = uni.getSystemInfoSync()
	Vue.prototype.uniWuiConfig = config
	Vue.prototype.$wuiUtils = utils
}
if (typeof window !== 'undefined' && window.Vue) {
	install(window.Vue);
};
export default {
	install
}
