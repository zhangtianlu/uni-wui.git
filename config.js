/**
 * uni-wui配置参数
 */

// uni-wui默认配置
const defaultOptions = {
	theme: {
		primary: '#fff',
		success: '#39B54A',
		info: '#0081FF',
		warning: '#FF9900',
	},
	zIndex: {
		navbar: 9999,
		loading: 9998,
		topTips: 9980,
		modal: 9970,
		popup: 9960,
		skeleton: 9950,
		dropdown: 9940,
		toast:9930
	}
}

export default defaultOptions
